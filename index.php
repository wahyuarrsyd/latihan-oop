<?php
    require_once('animal.php');
    require_once('frog.php');
    require_once('ape.php');
    $Animal = new animal ("shaun");
    echo "Name : " .$Animal->name . "<br>";
    echo "legs : " .$Animal->legs . "<br>";
    echo "cold blooded : " .$Animal->cold_blooded . "<br><br>";

    $frog = new frog("buduk");
    echo "Name : " .$frog->name . "<br>";
    echo "legs : " .$frog->legs . "<br>";
    echo "cold blooded : " .$frog->cold_blooded . "<br>";
    echo "Jump :  ". $frog->Lompat() . "<br><br>";

    $ape = new ape("Kera sakti");
    echo "Name : " .$ape->name . "<br>";
    echo "legs : " .$ape->legs . "<br>";
    echo "cold blooded : " .$ape->cold_blooded . "<br>";
    echo "Yell :  ". $ape->Suara() . "<br><br>";
    
    
?>